import React, { useState, useCallback } from 'react'
import Link from 'next/link'
import { Row, Col, Menu, Input, Icon, Drawer, Button } from 'antd'
import styled from 'styled-components';
// import Lottie from 'react-lottie'
// import * as animationData from './calling-animation';

// import bg from '/bg1.jpg';

const Nav = () => {
  const [visible, setVisible] = useState(false);
  const [activeTab, setActiveTab] = useState('home');
  const handleClickMenu = useCallback((e) => {
    setActiveTab(e.key);
  }, [activeTab])
  return (
    <div>
      <Header type="flex" justify="start" align="middle">
        {/* menu inline */}
        <Col xs={1} sm={1} md={0} lg={0} style={{ paddingLeft: '10px', fontSize: 18}}>
          <Icon onClick={() => setVisible(true)} type="menu-unfold" />
        </Col>
        {/*  */}
        <Col xs={16} sm={9} md={3} lg={5} xl={6} className="header-logo">
          logo
        </Col>
        {/* <Col span={18} className="header-menu">
          <Row type="flex" justify="start" align="middle"> */}
        <Col xs={0} sm={0} md={13} lg={13} xl={12}>
          <Menu onClick={handleClickMenu} style={{ background: "#ddd", borderLeft: '1px solid #c6c2c2' }} defaultSelectedKeys="home" selectedKeys={activeTab} mode="horizontal">
            <Menu.Item key="home">Trang Chủ</Menu.Item>
            <Menu.Item key="hot">Host</Menu.Item>
            <Menu.Item key="new">Hàng mới</Menu.Item>
            <Menu.Item key="best">Bán chạy nhất</Menu.Item>
            <Menu.Item key="km">Khuyến mại</Menu.Item>
          </Menu>
        </Col>
        <Col xs={0} sm={8} md={3} lg={3} style={{}}>
          <Input prefix={<Icon type="search" />} />
        </Col>
        <Col xs={6} sm={5} md={3} lg={3} style={{ paddingLeft: '10px' }}>
          <span >
            <Icon type="phone" style={{
              marinRight: "4px",
            }}></Icon>
            hot line
          </span>
        </Col>
        {/*  */}
        <Drawer
          title="Basic Drawer"
          placement="left"
          // closable={false}
          onClose={() => setVisible(false)}
          visible={visible}
        >
          <LeftMenu />
          {/* <RightMenu /> */}
        </Drawer>
      </Header>
    </div>
  );
}

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

const LeftMenu = () => {
  return (
    <Menu mode="inline">
      <Menu.Item key="mail">
        <a href="">Home</a>
      </Menu.Item>
      <SubMenu title={<span>Blogs</span>}>
        <MenuItemGroup title="Item 1">
          <Menu.Item key="setting:1">Option 1</Menu.Item>
          <Menu.Item key="setting:2">Option 2</Menu.Item>
        </MenuItemGroup>
        <MenuItemGroup title="Item 2">
          <Menu.Item key="setting:3">Option 3</Menu.Item>
          <Menu.Item key="setting:4">Option 4</Menu.Item>
        </MenuItemGroup>
      </SubMenu>
      <Menu.Item key="alipay">
        <a href="">Contact Us</a>
      </Menu.Item>
    </Menu>
  );
}

const Header = styled(Row)`
      /* height: 10px; */
      background: #ddd;
    
      position: relative;
      z-index: 10;
      height: 3rem;
      max-width: 100%;
      /* background: #fff; */
      -webkit-box-shadow: 0 2px 8px #f0f1f2;
      box-shadow: 0 2px 8px #f0f1f2;
      /* color:  */
      /* background-image: url('/bg1.jpg'); */
    `;

export default Nav
