import React from 'react'
import ReactDOM from 'react-dom'
import { useSpring, animated } from 'react-spring'
// import './styles.css'
import styled from 'styled-components';
import Lottie from 'react-lottie';
import animationData from './7286-mountain-with-sun.json';


function Card({ children }) {
  return (
    <CardStyled>
      {children}
    </CardStyled>
  )
}

export default Card;

const CardStyled = styled(animated.div)`
  width: 90%;
  height: 90%;

  position: absolute;
  box-shadow: 0px 5px 20px -5px rgba(0, 0, 0, 0.2);
  transition: box-shadow 0.5s;
  will-change: transform;
  transition: transform .35s ease-in-out;
  z-index: 1;

  &:hover {
    box-shadow: 0px 30px 100px -10px rgba(0, 0, 0, 0.1);
    transform: matrix(1.1, 0, 0, 1.1, 0, -10);
    z-index: 20;
  }

`