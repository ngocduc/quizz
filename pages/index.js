import { Layout, Menu, Row, Col } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import { useState } from 'react';

const { Header, Sider, Content } = Layout;

const SiderDemo = () => {
  const [collapsed, setCollapsed] = useState(false);

  const toggle = () => {
    setCollapsed(!collapsed)
  };

  return (
    <Layout>
      <Header 
      theme='light'
      style={{
        position: 'fixed',
        zIndex: 1, width: '100%'
      }}>
        <Row>
          {/* icon */}
          <Col style={{ background: '#ddd' }} xs={{ span: 20 }} sm={{ span: 4 }} md={{ span: 4 }}>
            Home
          </Col>
          {/* search */}
          <Col style={{ background: '#ddd' }} xs={{ span: 0 }} sm={{ span: 6 }} md={{ span: 20 }}>
            Col
          </Col>
          {/* menu */}
          <Col style={{ background: '#ddd' }} xs={{ span: 0 }} sm={{ span: 10 }} md={{ span: 20 }}>
            Col
          </Col>
          {/* right */}
          <Col style={{ background: '#ddd' }} xs={{ span: 4 }} sm={{ span: 2 }} md={{ span: 2 }}>
            Col
          </Col>
        </Row>
      </Header>
      <Content
        className="site-layout-background"
        style={{
          margin: '24px 16px',
          padding: 24,
          minHeight: 280,
        }}
      >
        {
          new Array(100).fill('').map(() => {
            return (
              <p> Content </p>
            )
          })
        }
      </Content>
    </Layout>
  );

}

export default SiderDemo;