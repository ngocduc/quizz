import React, { useState, useEffect } from 'react';
import { useRequest, Loading } from '../handler/request';
import { Row, Col, Menu, Input, Icon } from 'antd'
import Layout from '../components/layout';

const Detail = (props) => {
	// const [data, isLoading, err] = useRequest('', []);
	const [count, setCount] = useState(1);
	const [money, setMoney] = useState(0);
	const [money_old, setMoneyold] = useState(0);
	const [content, setContent] = useState("");
	const text1 =
		"Mặt nạ Thương hiệu JMSOLUTION \n Dành cho loại da Mọi loại da \n Dung tích (ml) 45 \n Xuất xứ Hàn Quốc";
	const text2 =
		"☁️☁️ Mặt nạ giấy JM_Solution Water Luminous Silky Cocoon Mask \n☁️☁️ 🍃JM Solution Water Luminous Silky Cocoon Mask 30ml giàu dưỡng chất, tái tạo cấu trúc cho da, làm trắng da. \n 🍃Chiết xuất tơ tằm giúp chống lão hóa cho da. \n 🍃Tăng sức đề kháng, cho da luôn khỏe mạnh từ bên trong. \n🍃Duy trì độ đàn hồi, làm trẻ hóa làn da. An toàn, thích hợp sử dụng cho nhiều loại da. . \n💌HƯỚNG DẪN SỬ DỤNG: Mặt nạ giấy JM Solution Water Luminous Silky Cocoon Mask 30ml được sử dụng đơn giản: \n 🔺Bước 1: Rửa mặt sạch. \n 🔺Bước 2: Lấy miếng mặt nạ dưỡng da ra, đắp lên mặt và để khoảng 15-20 phút rồi bỏ miếng mặt nạ ra. \n🔺Bước 3: Nhẹ nhàng vỗ nhẹ để các dưỡng chất thấm sâu vào da";

	return (
		<Layout>
			<Loading isLoading={false} err={false} height='70vh'>
			<div>
				{/* <Layout /> */}
				<Row>
					<Col xs={22} sm={22} md={10} lg={10} xl={6}
						style={{ marginRight: 50, marginBottom: 0 }}>

						<img
							style={{ margin: 20 }}
							src={'http://img.daidoanket.vn/images/uploads/2019/thang_7/6/4.jpg'}
							width="100%"
						></img>
					</Col>

					<Col
						xs={24}
						sm={24}
						md={10}
						lg={12}
						xl={12}
						style={{ marginLeft: 20, maginBottom: 0 }}
					>

						<font size='6'>{content}</font>
						<Row style={{ marginTop: 10 }}>
							<Col xs={5} sm={5} md={6} lg={3} xl={3} style={{ marginRight: 30 }}>
								<strike>

									<p style={{ fontSize: 20 }}>
										{money_old.toLocaleString("it-IT", {
											style: "currency",
											currency: "VND"
										})}
									</p>

								</strike>
							</Col>
							<Col xs={5} sm={5} md={6} lg={3} xl={3} style={{ marginRight: 30 }}>


								<p style={{ color: "red", fontSize: 20 }}>
									{money.toLocaleString("it-IT", {
										style: "currency",
										currency: "VND"
									})}
								</p>

							</Col>

							<Col xs={5} sm={5} md={6} lg={3} xl={3}>


								<p style={{ color: "red", fontSize: 20 }}>
									Giảm:
              {(((money_old - money) * 100) / money_old).toFixed(2)}%
            </p>

							</Col>

						</Row>

						<Row>
							<Col xs={5} sm={5} md={6} lg={3} xl={3}>
								<p style={{ fontSize: 18 }}>
									Số lượng
              </p>
							</Col>
							<Col xs={5} sm={5} md={6} lg={3} xl={3}>
								<button onClick={() => setCount(count - 1)} style={{ width: 30 }}>
									-
              </button>
							</Col>
							<Col xs={5} sm={5} md={6} lg={3} xl={3}>
								<h3>{count}</h3>
							</Col>

							<Col xs={5} sm={5} md={6} lg={3} xl={3}>
								<button onClick={() => setCount(count + 1)} style={{ width: 30 }}>
									+
              </button>
							</Col>
						</Row>
						<p style={{ fontSize: 18, fontWeight: 'bold' }}>
							Giá Tiền:
            {(count * money).toLocaleString("it-IT", {
								style: "currency",
								currency: "VND"
							})}
						</p>

						<button style={{ minWidth: 100, border: "1px red", padding: 10 }}>
							Thêm Vào Giỏ Hàng
          </button>

						<button
							style={{
								minWidth: 100,
								margin: 20,
								background: "red",
								border: "1px red",
								color: "white",
								padding: 10
							}}
						>
							Mua Hàng
          </button>
					</Col>
				</Row>
				<div>
					<div style={{ margin: 20 }}>
						<h3 style={{ background: "rgba(0,0,0,.1", padding: 10 }}>
							Chi Tiết Sản Phẩm
          </h3>
						<p>
							{" "}
							{text1.split("\n").map((line, i, arr) => {
								const line2 = <span key={i}> {line} </span>;

								if (i === arr.length - 1) {
									return line2;
								} else {
									return [line2, <br key={i + "br"} />];
								}
							})}
						</p>
					</div>
					<div style={{ margin: 20 }}>
						<h3 style={{ background: "rgba(0,0,0,.1", padding: 10 }}>
							Mô Tả Sản Phẩm
          </h3>
						<p>
							{text2.split("\n").map((line, i, arr) => {
								const line2 = <span key={i}> {line} </span>;

								if (i === arr.length - 1) {
									return line2;
								} else {
									return [line2, <br key={i + "br"} />];
								}
							})}
						</p>
					</div>
				</div>
			</div>
			</Loading>
		</Layout>
	);
}

export default Detail;